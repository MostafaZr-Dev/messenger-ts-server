import { Application } from 'express'
import * as express from 'express'
import * as cors from 'cors'

export default (app:Application) => {
  app.use(cors())
  app.use(express.json())
}
