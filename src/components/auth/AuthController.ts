import { NextFunction, Request, Response } from 'express'

import AuthService from './AuthService'
import TokenService from '@services/TokenService'
import ServerException from '@components/exceptions/ServerException'
import UnAuthorizedException from '@components/exceptions/UnAuthorizedException'
import UserTransformer from '@components/users/UserTransformer'
import ChatService from '@components/chat/ChatService'

export default class AuthController {
  private readonly authService: AuthService
  private readonly userTransformer: UserTransformer
  private readonly chatService: ChatService

  public constructor () {
    this.init = this.init.bind(this)
    this.register = this.register.bind(this)
    this.authenticate = this.authenticate.bind(this)

    this.authService = new AuthService()
    this.userTransformer = new UserTransformer()
    this.chatService = new ChatService()
  }

  public async init (req:Request, res:Response, next:NextFunction) : Promise<void> {
    try {
      const { uid } = req.body

      const user = await this.authService.findUserByHash(uid)

      if (!user) {
        throw new UnAuthorizedException('unauthorized access!')
      }

      const recentChats = await this.chatService.fetchRecentChats(uid)

      const transformedUser = this.userTransformer.transform(user)

      res.send({
        success: true,
        user: transformedUser,
        recentChats
      })
    } catch (error) {
      next(error)
    }
  }

  public async register (req:Request, res:Response, next:NextFunction):Promise<void> {
    try {
      const {
        fullName,
        email,
        password
      } = req.body

      const newUser = await this.authService.register(
        fullName,
        email,
        password
      )

      if (!newUser) {
        throw new ServerException('امکان ثبت نام وجود ندارد! بعدا تلاش کنید.')
      }

      res.send({
        success: true,
        message: 'ثبت نام شما با موفقیت انجام شد.'
      })
    } catch (error) {
      next(error)
    }
  }

  public async authenticate (req:Request, res:Response, next:NextFunction):Promise<void> {
    try {
      const {
        email,
        password
      } = req.body

      const user = await this.authService.authenticate(email, password)

      if (!user) {
        throw new UnAuthorizedException('ایمیل یا رمز عبور اشتباه است!')
      }

      const token = TokenService.sign({
        uid: user.hash
      })

      res.send({
        success: true,
        message: 'ورود با موفقیت انجام شد.',
        token
      })
    } catch (error) {
      next(error)
    }
  }
}
