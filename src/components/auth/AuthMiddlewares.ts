import { NextFunction, Request, Response } from 'express'
import { validationResult, checkSchema } from 'express-validator'

import ValidationException from '@components/exceptions/ValidationException'

type ValidationErrors = {
    [key:string]: string[]
}

export const validtor = (req:Request, res:Response, next:NextFunction) => {
  try {
    const errors = validationResult(req)

    const extractErrors: ValidationErrors = {}

    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        const field = error.param

        if (field in extractErrors) {
          extractErrors[field].push(error.msg)
          return
        }

        extractErrors[field] = [error.msg]
      })

      throw new ValidationException('Invalid input!', {
        errors: extractErrors
      })
    }

    next()
  } catch (error) {
    next(error)
  }
}

export const validateRegisterData = checkSchema({
  fullName: {
    isEmpty: {
      errorMessage: 'نام و نام خانوادگی را وارد کنید!',
      negated: true
    },
    isLength: {
      options: {
        min: 3
      },
      errorMessage: 'نام باید حداقل 3 کاراکتر باشد!'
    }
  },
  email: {
    isEmpty: {
      errorMessage: 'ایمیل خود را وارد کنید!',
      negated: true
    },
    isEmail: {
      errorMessage: 'ایمیل نامعتبر!',
      bail: true
    }
  },
  password: {
    isEmpty: {
      errorMessage: 'رمز عبور خود را وارد کنید!',
      negated: true
    }
  }
})

export const validateLoginData = checkSchema({
  email: {
    isEmpty: {
      errorMessage: 'ایمیل خود را وارد کنید!',
      negated: true
    },
    isEmail: {
      errorMessage: 'ایمیل نامعتبر!',
      bail: true
    }
  },
  password: {
    isEmpty: {
      errorMessage: 'رمز عبور خود را وارد کنید!',
      negated: true
    }
  }
})
