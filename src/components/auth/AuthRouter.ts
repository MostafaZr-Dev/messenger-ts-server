import { Router } from 'express'

import AuthController from './AuthController'
import checkToken from '@middlewares/checkToken'
import * as authMiddleware from './AuthMiddlewares'

const authRouter = Router()
const authControllerInstance = new AuthController()

authRouter.get(
  '/init',
  checkToken,
  authControllerInstance.init
)

authRouter.post(
  '/register',
  authMiddleware.validateRegisterData,
  authMiddleware.validtor,
  authControllerInstance.register
)

authRouter.post(
  '/login',
  authMiddleware.validateLoginData,
  authMiddleware.validtor,
  authControllerInstance.authenticate
)

export default authRouter
