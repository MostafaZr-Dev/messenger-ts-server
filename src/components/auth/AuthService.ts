import IUser from '@components/users/model/IUser'
import IUserRepository from '@components/users/repository/IUserRepository'
import UserMysqlRepository from '@components/users/repository/UserMysqlRepository'
import HashService from '@services/HashService'

export default class AuthService {
    private readonly userRepository: IUserRepository
    public constructor () {
      this.userRepository = new UserMysqlRepository()
    }

    public async register (fullName:string, email:string, password: string):Promise<any> {
      const newUser = await this.userRepository.create({
        fullName,
        email,
        password: HashService.hashPassword(password),
        hash: HashService.generateID()
      })

      return newUser
    }

    public async authenticate (email:string, password: string):Promise<IUser | null> {
      const user = await this.userRepository.findByEmail(email)

      if (!user) {
        return null
      }

      const isValidPassword = HashService.comparePassword(user.password, password)

      if (!isValidPassword) {
        return null
      }

      return user
    }

    public async findUserByHash (hash: string): Promise<IUser | null> {
      const user = await this.userRepository.findByHash(hash)

      if (!user) {
        return null
      }

      return user
    }
}
