import { NextFunction, Request, Response } from 'express'

import IUserRepository from '@components/users/repository/IUserRepository'
import UserMysqlRepository from '@components/users/repository/UserMysqlRepository'
import UnAuthorizedException from '@components/exceptions/UnAuthorizedException'
import ChatMongoRepository from './repository/ChatMongoRepository'
import IChatRepository from './repository/IChatRepository'
import UserTransformer from '@components/users/UserTransformer'
import ServerException from '@components/exceptions/ServerException'
import ChatService from './ChatService'
import UnprocessableEntity from '@components/exceptions/UnprocessableEntity'

export default class ChatController {
    private readonly usersRepository: IUserRepository
    private readonly chatRepository: IChatRepository
    private readonly usersTransformer: UserTransformer
    private readonly chatService: ChatService

    public constructor () {
      this.init = this.init.bind(this)
      this.saveMessage = this.saveMessage.bind(this)
      this.finishChat = this.finishChat.bind(this)

      this.usersRepository = new UserMysqlRepository()
      this.chatRepository = new ChatMongoRepository()
      this.usersTransformer = new UserTransformer()
      this.chatService = new ChatService()
    }

    public async init (req:Request, res:Response, next:NextFunction) {
      try {
        const { uid, participant: participantHash } = req.body

        const user = await this.usersRepository.findByHash(uid)
        const participant = await this.usersRepository.findByHash(participantHash)

        if (!user || !participant) {
          throw new UnAuthorizedException('اطلاعات کاربران وارد شده صحیح نیست!')
        }

        let chat = await this.chatRepository.findByUserHash(uid)

        const userDetails = this.usersTransformer.buildChatUserDetails(user)
        const participantDetails = this.usersTransformer.buildChatUserDetails(participant)

        if (!chat) {
          chat = await this.chatRepository.create({
            participants: [
              {
                hash: user.hash,
                details: userDetails
              },
              {
                hash: participant.hash,
                details: participantDetails
              }
            ]
          })
        }

        if (!chat) {
          throw new ServerException('درحال حاضر سیستم قادر به ایجاد گفتگو نیست!')
        }

        res.send({
          success: true,
          chat: {
            id: chat._id,
            participants: {
              [user.hash]: userDetails,
              [participant.hash]: participantDetails
            }
          }
        })
      } catch (error) {
        next(error)
      }
    }

    public async saveMessage (req:Request, res:Response, next:NextFunction): Promise<void> {
      try {
        const { content, type, sender, createdAt } = req.body

        const updateResult = await this.chatService.updateChatMessages(
          sender,
          {
            sender,
            content,
            type,
            createdAt
          }
        )

        if (!updateResult) {
          throw new UnprocessableEntity('گفتگویی با این مشخصات وجود ندارد!')
        }

        res.send({
          success: true,
          message: 'پیام با موفقیت ثبت شد'
        })
      } catch (error) {
        next(error)
      }
    }

    public async finishChat (req: Request, res: Response, next:NextFunction): Promise<void> {
      try {
        const { id: chatID } = req.params

        const finishResult = await this.chatService.finishChat(chatID)

        if (!finishResult) {
          throw new UnprocessableEntity('گفتگویی با این مشخصات وجود ندارد!')
        }

        res.send({
          success: true,
          message: 'گفتگو با موفقیت به پایان رسید'
        })
      } catch (error) {
        next(error)
      }
    }

    public async activeChat (req:Request, res:Response, next:NextFunction):Promise<void> {
      try {
        const { id: chatID } = req.params

        const chat = await this.chatRepository.findOne(chatID, {
          messages: {
            $slice: -50
          }
        })

        if (!chat) {
          throw new UnprocessableEntity('گفتگویی با این مشخصات وجود ندارد!')
        }

        const chatParticipants: {[key:string]: object} = {}

        chat.participants.forEach(pr => {
          chatParticipants[pr.hash] = {
            ...pr.details
          }
        })

        res.send({
          success: true,
          chat: {
            currentChat: {
              id: chatID,
              participants: chatParticipants,
              isFinished: chat.isFinished
            },
            currentChatMessages: chat.messages
          }
        })
        console.log(chatID)
      } catch (error) {
        next(error)
      }
    }
}
