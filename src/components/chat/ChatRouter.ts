import { Router } from 'express'

import checkToken from '@middlewares/checkToken'
import ChatController from './ChatController'

const chatRouter = Router()
const chatControllerInstance = new ChatController()

chatRouter.post('/', checkToken, chatControllerInstance.init)
chatRouter.post('/messages', checkToken, chatControllerInstance.saveMessage)
chatRouter.patch('/:id/finish', checkToken, chatControllerInstance.finishChat)
chatRouter.get('/:id/active', checkToken, chatControllerInstance.activeChat)

export default chatRouter
