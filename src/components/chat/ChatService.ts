import IChatRepository from './repository/IChatRepository'
import ChatMongoRepository from './repository/ChatMongoRepository'
import IMessage from './model/IMessage'
import IRecentChat from './model/IRecentChat'

export default class ChatService {
    private readonly chatRepository: IChatRepository

    public constructor () {
      this.chatRepository = new ChatMongoRepository()
    }

    public async updateChatMessages (userHash:string, message: Partial<IMessage>): Promise<boolean> {
      const chat = await this.chatRepository.findByUserHash(userHash)

      if (!chat) {
        return false
      }

      const isUpdateMessages = await this.chatRepository.updateChatMessages(
        chat._id,
        message
      )

      if (!isUpdateMessages) {
        return false
      }

      return true
    }

    public async finishChat (chatID:string):Promise<boolean> {
      const chat = await this.chatRepository.findOne(chatID)

      if (!chat) {
        return false
      }

      if (chat && chat.isFinished) {
        return true
      }

      await this.chatRepository.updateOne({
        _id: chat._id
      }, {
        isFinished: true
      })

      return true
    }

    public async fetchRecentChats (hash: string, limit: number = 10) : Promise<IRecentChat[]> {
      const recentChats = await this.chatRepository.findRecentChatsByUserHash(hash, limit)

      if (!recentChats) {
        return []
      }

      return recentChats.map(recentChat => {
        const user = recentChat.participants.filter(user => user.hash !== hash).pop()

        const lastMessage = recentChat.messages.length > 0
          ? recentChat.messages.pop()
          : null

        return {
          id: recentChat._id,
          user,
          lastMessage,
          createdAt: recentChat.createdAt
        }
      })
    }
}
