import { Schema, model } from 'mongoose'

import IChat from './IChat'
import { messageSchema } from './Message'

const chatSchema: Schema = new Schema({
  participants: { type: Object, required: true },
  messages: { type: [messageSchema], default: null },
  isFinished: { type: Boolean, required: true, default: false },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  }
})

export default model<IChat>('chats', chatSchema)
