import { Document } from 'mongoose'

import IMessage from './IMessage'

export type Participant = {
    hash: string;
    details: {
        hash: string;
        fullName: string;
        avatar: string;
    }
}

export default interface IChat extends Document {
    participants: Participant[];
    messages: IMessage[];
    isFinished: boolean;
    createdAt: Date;
}
