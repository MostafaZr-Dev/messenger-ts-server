import { Document } from 'mongoose'

import MessageType from './MessageType'

export default interface IMessage extends Document{
    sender: string;
    content: string;
    type: MessageType;
    createdAt: Date;
}
