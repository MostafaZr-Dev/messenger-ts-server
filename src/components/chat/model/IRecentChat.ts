import { Participant } from './IChat'
import IMessage from './IMessage'

export default interface IRecentChat {
    id?:string;
    user?: Participant;
    lastMessage?: IMessage | null;
    createdAt?: Date;
}
