import { Schema } from 'mongoose'

import MessageType from './MessageType'

export const messageSchema: Schema = new Schema({
  sender: { type: String, required: true },
  type: { type: MessageType, default: MessageType.TEXT, required: true },
  content: { type: String, required: true },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  }
})
