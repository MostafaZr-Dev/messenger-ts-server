/* eslint-disable no-unused-vars */

enum IMessageType {
    TEXT,
    FILE,
    AUDIO
}

export default IMessageType
