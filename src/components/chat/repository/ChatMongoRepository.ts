import { FilterQuery } from 'mongoose'

import IChat from '../model/IChat'
import IChatRepository from './IChatRepository'
import ChatModel from '../model/Chat'
import IMessage from '../model/IMessage'

export default class ChatMongoRepository implements IChatRepository {
  public async findRecentChatsByUserHash (hash: string, limit: number): Promise<IChat[]> {
    return ChatModel.find({
      participants: {
        $elemMatch: {
          hash
        }
      }
    }, {
      messages: {
        $slice: -1
      }
    }, {
      limit
    }).exec()
  }

  public async updateChatMessages (chatID: string, message: Partial<IMessage>): Promise<boolean> {
    const updateResult = await ChatModel.updateOne({
      _id: chatID
    }, {
      $push: {
        messages: message
      }
    })

    if (!updateResult.nModified) {
      return false
    }

    return true
  }

  public async findByUserHash (hash: string): Promise<IChat | null> {
    return ChatModel.findOne({
      participants: {
        $elemMatch: {
          hash
        }
      },
      isFinished: false
    })
  }

  public async findOne (id: string, projection?: any): Promise<IChat | null> {
    return ChatModel.findOne({ _id: id }, projection)
  }

  public async findAll (params: any, projection?: any): Promise<IChat[]> {
    throw new Error('Method not implemented.')
  }

  public async create (params: any): Promise<IChat> {
    const newChat = new ChatModel({
      ...params
    })

    return newChat.save()
  }

  public async updateOne (where: Partial<IChat>, updatedData: Partial<IChat>): Promise<boolean> {
    const updateResult = await ChatModel.updateOne(where as FilterQuery<IChat>, updatedData)

    if (!updateResult.nModified) {
      return false
    }

    return true
  }
}
