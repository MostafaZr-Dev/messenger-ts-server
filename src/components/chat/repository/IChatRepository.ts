import IRepository from '@components/contracts/IRepository'
import IChat from '../model/IChat'
import IMessage from '../model/IMessage'

export default interface IChatRepository extends IRepository<IChat>{
    findByUserHash(hash: string): Promise<IChat | null>
    updateChatMessages(chatID:string, message:Partial<IMessage>): Promise<boolean>
    findRecentChatsByUserHash(hash:string, limit:number) : Promise<IChat[]>
}
