
export default interface IRepository<T> {
    findOne(id: string, projection?: any): Promise<T | null>
    findAll(params: any, projection?: any): Promise<T[]>
    create(params: any): Promise<T>
    updateOne(where:Partial<T>, updatedData:Partial<T>): Promise<boolean>
}
