
export default class Exception extends Error {
    readonly status: number;
    readonly message: string;
    readonly name: string;
    readonly data: any;

    constructor (status: number, message: string, data?: any) {
      super(message)
      this.status = status
      this.message = message
      this.name = this.constructor.name
      this.data = data
    }
}
