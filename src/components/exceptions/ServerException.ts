import Exception from './Exception'

export default class ServerException extends Exception {
  constructor (message: string, data?:any) {
    super(500, message, data)
  }
}
