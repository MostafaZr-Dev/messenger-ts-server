import Exception from './Exception'

export default class UnAuthorizedException extends Exception {
  constructor (message: string, data?:any) {
    super(401, message, data)
  }
}
