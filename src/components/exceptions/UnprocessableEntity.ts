import Exception from './Exception'

export default class UnprocessableEntity extends Exception {
  constructor (message: string, data?:any) {
    super(422, message, data)
  }
}
