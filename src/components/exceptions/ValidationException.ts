import Exception from './Exception'

export default class ValidationException extends Exception {
  constructor (message: string, data?:any) {
    super(400, message, data)
  }
}
