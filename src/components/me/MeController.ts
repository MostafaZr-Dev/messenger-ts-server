import { NextFunction, Request, Response } from 'express'

import MeService from './MeServices'
import ServerException from '@components/exceptions/ServerException'
import { UsersService } from '@components/users/UsersService'

export default class MeController {
    private readonly meService: MeService
    private readonly usersService: UsersService

    public constructor () {
      this.setLocation = this.setLocation.bind(this)

      this.meService = new MeService()
      this.usersService = new UsersService()
    }

    public async setLocation (req:Request, res:Response, next:NextFunction): Promise<void> {
      try {
        const { uid, address, latitude, longitude } = req.body

        const newOnlineUser = await this.meService.addToOnlineUsers(
          uid,
          latitude,
          longitude
        )

        if (!newOnlineUser) {
          throw new ServerException('مشکلی بوجود آمده! بعدا امتحان کنید!')
        }

        await this.usersService.updateUserAddress(uid, address)

        res.send({
          success: true
        })
      } catch (error) {
        next(error)
      }
    }
}
