import { Router } from 'express'

import MeController from './MeController'
import checkToken from '@middlewares/checkToken'

const meRouter = Router()
const meControllerInstance = new MeController()

meRouter.post(
  '/location',
  checkToken,
  meControllerInstance.setLocation
)

export default meRouter
