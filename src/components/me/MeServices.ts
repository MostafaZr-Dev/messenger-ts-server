import IOnlineUsersRepository from '@components/online-users/repository/IOnlineUsersRepository'
import OnlineUsersMongoRepository from '@components/online-users/repository/OnlineUsersMongoRepository'

export default class MeService {
    private readonly onlineUsersRepository: IOnlineUsersRepository
    public constructor () {
      this.onlineUsersRepository = new OnlineUsersMongoRepository()
    }

    public async addToOnlineUsers (userHash:string, latitude: number, longitude: number) {
      const newOnlineUser = await this.onlineUsersRepository.create({
        user: userHash,
        location: {
          type: 'Point',
          cordinates: [
            latitude,
            longitude
          ]
        }
      })

      return newOnlineUser
    }
}
