import { Document } from 'mongoose'

type OnlineUser = {
    hash: string;
    avatar: string;
    fullName: string;
}

export default interface IOnlineUser extends Document {

    user: OnlineUser;
    location: {
        type: string;
        coordinates: number[]
    }

}
