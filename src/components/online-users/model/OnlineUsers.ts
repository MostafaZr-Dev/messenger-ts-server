import { Schema, model } from 'mongoose'

import IOnlineUser from './IOnlineUser'

const pointSchema: Schema = new Schema({
  type: {
    type: String,
    enum: ['Point'],
    required: true
  },
  coordinates: {
    type: [Number],
    required: true
  }
})

const onlineUserSchema: Schema = new Schema({

  user: { type: Schema.Types.Mixed, required: true },
  location: {
    type: pointSchema,
    index: '2dsphere'
  }
}, {
  collection: 'online_users'
})

export default model<IOnlineUser>('online_users', onlineUserSchema)
