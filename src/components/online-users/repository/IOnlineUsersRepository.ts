import IRepository from '@components/contracts/IRepository'
import IOnlineUser from '../model/IOnlineUser'

export default interface IOnlineUsersRepository extends IRepository<IOnlineUser>{
    findByHash(hash:string): Promise<IOnlineUser | null>
    removeByHash(hash:string): Promise<boolean>
}
