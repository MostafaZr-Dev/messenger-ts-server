import IOnlineUser from '../model/IOnlineUser'
import OnlineUsersModel from '../model/OnlineUsers'
import IOnlineUsersRepository from './IOnlineUsersRepository'

export default class OnlineUsersMongoRepository implements IOnlineUsersRepository {
  public async removeByHash (hash: string): Promise<boolean> {
    const result = await OnlineUsersModel.deleteOne({
      'user.hash': {
        $eq: hash
      }
    }).exec()

    return !!result.deletedCount
  }

  public async findByHash (hash: string): Promise<IOnlineUser | null> {
    return OnlineUsersModel.findOne({
      'user.hash': {
        $eq: hash
      }
    })
  }

  public async findOne (id: string, projection?: any): Promise<IOnlineUser | null> {
    throw new Error('Method not implemented.')
  }

  public async findAll (params: any, projection?: any): Promise<IOnlineUser[]> {
    const onlineUsersQueryParams = { ...params }

    if (params.location) {
      onlineUsersQueryParams.location = {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates: params.location
          },
          $maxDistance: 5000
        }
      }
    }

    const onlineUsersQuery = OnlineUsersModel.find(onlineUsersQueryParams)

    if (projection) {
      onlineUsersQuery.select({
        ...projection
      })
    }

    return onlineUsersQuery.exec()
  }

  public async create (params: any): Promise<IOnlineUser> {
    const newOnlineUser = new OnlineUsersModel({
      ...params
    })

    await newOnlineUser.save()

    await OnlineUsersModel.on('index', (error) => {
      console.log('error index onlineUsers', error)
    })

    return newOnlineUser
  }

  public async updateOne (where: Partial<IOnlineUser>, updatedData: Partial<IOnlineUser>): Promise<boolean> {
    throw new Error('Method not implemented.')
  }
}
