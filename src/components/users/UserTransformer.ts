import ITransformer from '@components/contracts/ITransformer'
import IUser from './model/IUser'
import { getAvatarUrl } from '@services/GravatarService'

export default class UserTransformer implements ITransformer<IUser> {
  public transform (item: IUser) {
    return {
      hash: item.hash,
      fullName: item.fullName,
      email: item.email,
      avatar: getAvatarUrl(item.email),
      address: item.address
    }
  }

  public collection (items: IUser[]) {
    return items.map(item => this.transform(item))
  }

  public buildChatUserDetails (item: IUser) {
    return {
      hash: item.hash,
      fullName: item.fullName,
      avatar: getAvatarUrl(item.email)
    }
  }
}
