import { Request, Response } from 'express'

export default class UsersController {
  constructor () {
    this.index = this.index.bind(this)
  }

  public async index (req: Request, res: Response): Promise<void> {
    res.send({
      success: true
    })
  }
}
