import { Router } from 'express'

import UsersController from './UsersController'

const usersControllerInstance = new UsersController()
const usersRouter = Router()

usersRouter.get('/', usersControllerInstance.index)

export default usersRouter
