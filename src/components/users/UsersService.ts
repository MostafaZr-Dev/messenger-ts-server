import IUserRepository from './repository/IUserRepository'
import UserMysqlRepository from './repository/UserMysqlRepository'

export class UsersService {
    private readonly usersRepository: IUserRepository

    public constructor () {
      this.usersRepository = new UserMysqlRepository()
    }

    public async updateUserAddress (hash:string, address: string): Promise<boolean> {
      return this.usersRepository.updateByHash(hash, {
        address
      })
    }
}
