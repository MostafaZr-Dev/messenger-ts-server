
export default interface IUser {
    id:number;
    hash: string;
    fullName: string;
    email: string;
    password: string;
    address: string;
    createdAt: Date;

}
