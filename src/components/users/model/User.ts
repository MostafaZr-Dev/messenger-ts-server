import { Model, Optional, DataTypes } from 'sequelize'

import sequlize from '@infrastructure/connection/sequelize'
import IUser from './IUser'

interface UserCreationAttributes extends Optional<IUser, 'id'> {}
interface UserInstance
  extends Model<IUser, UserCreationAttributes>,
  IUser {}

const UserModel = sequlize.define<UserInstance>('users', {
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true
  },
  hash: {
    type: DataTypes.STRING,
    allowNull: false
  },
  fullName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  password: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  address: {
    type: DataTypes.STRING(250),
    allowNull: true
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: new Date()
  }
})

export default UserModel
