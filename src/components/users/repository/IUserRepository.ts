import IRepository from '@components/contracts/IRepository'
import IUser from '../model/IUser'

export default interface IUserRepository extends IRepository<IUser>{
    findByHash(hash: string): Promise<IUser | null>
    findByEmail(email: string): Promise<IUser | null>
    updateByHash(hash: string, updateData: Partial<IUser>): Promise<boolean>
}
