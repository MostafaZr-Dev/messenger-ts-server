import IUser from '../model/IUser'
import IUserRepository from './IUserRepository'
import UserModel from '../model/User'

export default class UserMysqlRepository implements IUserRepository {
  public async updateByHash (hash: string, updateData: Partial<IUser>): Promise<boolean> {
    const [isUpdated] = await UserModel.update({
      address: updateData.address
    }, {
      where: {
        hash
      }
    })

    return !!isUpdated
  }

  public async findByEmail (email: string): Promise<IUser | null> {
    return UserModel.findOne({
      where: {
        email
      }
    })
  }

  public async findByHash (hash: string): Promise<IUser | null> {
    return UserModel.findOne({
      where: {
        hash
      }
    })
  }

  public async findOne (id: string, projection?: any): Promise<IUser | null> {
    throw new Error('Method not implemented.')
  }

  public async findAll (params: any, projection?: any): Promise<IUser[]> {
    throw new Error('Method not implemented.')
  }

  public async create (params: any): Promise<IUser> {
    return UserModel.create(params)
  }

  public async updateOne (where: Partial<IUser>, updatedData: Partial<IUser>): Promise<boolean> {
    throw new Error('Method not implemented.')
  }
}
