import * as mongoose from 'mongoose'

mongoose.connection.once('open', () => {
  console.log('mongo connection is open...')
})

mongoose.connection.on('error', (err) => {
  console.log('failed to connected!', err.message)
})

const startMongo = () => {
  mongoose.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
}

export default startMongo
