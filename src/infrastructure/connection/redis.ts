import * as redis from 'redis'

export const redisClient = redis.createClient(
  parseInt(process.env.REDIS_PORT as string),
  process.env.REDIS_HOST as string
)

export default () => {
  redisClient.on('connect', () => {
    console.log('redis connected...')
  })

  redisClient.on('error', (error) => {
    console.error(error)
  })
}
