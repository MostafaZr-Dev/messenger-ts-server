import { Sequelize } from 'sequelize'

const sequelize = new Sequelize(
  `mysql://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
)

export const mysqlConnect = async () => {
  try {
    await sequelize.authenticate()
    console.log('mysql connected...')
  } catch (error) {
    console.error('Unable to connect to the mysql:', error)
  }
}

export default sequelize
