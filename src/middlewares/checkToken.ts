import { NextFunction, Request, Response } from 'express'
import { JwtPayload } from 'jsonwebtoken'

import TokenService from '@services/TokenService'
import UnAuthorizedException from '@components/exceptions/UnAuthorizedException'

export default (req:Request, res:Response, next:NextFunction) => {
  const token = TokenService.findToken(req)

  if (!token) {
    throw new UnAuthorizedException('Invalid Token!')
  }

  const tokenData = TokenService.verify(token)

  if (!tokenData) {
    throw new UnAuthorizedException('Invalid Token!')
  }

  Object.assign(req.body, { uid: (tokenData as JwtPayload).uid })

  next()
}
