import { NextFunction, Request, Response } from 'express'

import Exception from '@components/exceptions/Exception'

export default (error:Exception, req:Request, res:Response, next:NextFunction) => {
  console.log(error)
  res.status(error.status).send({
    status: error.status,
    code: error.name,
    message: error.message,
    ...(error.data && {
      data: error.data
    })
  })
}
