import { NextFunction, Request, Response } from 'express'

export default (req: Request, res: Response, next: NextFunction) => {
  res.status(404).send({
    status: 404,
    error: 'Not Found',
    message: 'Requested resource could not be found!'
  })
}
