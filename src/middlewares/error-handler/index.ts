import { Application } from 'express'

import ExceptionHandler from './ExceptionHandler'
import NotFounHandler from './NotFounHandler'

export default (app:Application) => {
  app.use(ExceptionHandler)
  app.use(NotFounHandler)
}
