import { PeerServer } from 'peer'
import * as fs from 'fs'
import * as path from 'path'

const privateKey = fs.readFileSync(path.resolve(process.cwd(), '.cert/key.pem'), 'utf-8')
const certificate = fs.readFileSync(path.resolve(process.cwd(), '.cert/cert.pem'), 'utf-8')

export default class PeerServerApp {
    private server
    private static instance: PeerServerApp | null = null

    public constructor (port:number, key:string, path: string) {
      this.server = PeerServer({
        port,
        path,
        key,
        ssl: {
          key: privateKey,
          cert: certificate
        }
      })
      this.server.on('connection', this.connectionHandler)
    }

    public static getPeerServer (port:number, key:string, path: string) {
      if (!PeerServerApp.instance) {
        PeerServerApp.instance = new PeerServerApp(port, key, path)
      }

      return PeerServerApp.instance
    }

    private connectionHandler (client:any) {
      console.warn(`peer client connected with ID: ${client.getId()}`)
    }
}
