import { Application, Router } from 'express'

import RouteEngine from './router'
import usersRouter from '@components/users/UsersRouter'
import authRouter from '@components/auth/AuthRouter'
import meRouter from '@components/me/MeRouter'
import chatRouter from '@components/chat/ChatRouter'

class RouteService {
    private app: Application;
    private router: RouteEngine;

    public constructor (app: Application) {
      this.app = app
      this.router = new RouteEngine()
      this.bindRouters()
    }

    public bindRouters () {
      this.router.registerRouter('/api/v1/users', usersRouter)
      this.router.registerRouter('/api/v1/auth', authRouter)
      this.router.registerRouter('/api/v1/me', meRouter)
      this.router.registerRouter('/api/v1/chat', chatRouter)
    }

    public run () {
      this.router.getRouters().forEach((router: Router, route: string) => {
        this.app.use(route, router)
      })
    }
}

export default RouteService
