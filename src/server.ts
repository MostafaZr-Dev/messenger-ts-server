/* eslint-disable import/first */
import 'module-alias/register'
import * as dotenv from 'dotenv'
dotenv.config()

import App from './app'
import PeerServerApp from './peer'
import SocketServer from './socket-server/SocketServer'
import Messenger from './socket-server/Messenger'
import { mysqlConnect } from '@infrastructure/connection/sequelize'
import mongoConnect from '@infrastructure/connection/mongoose'
import redisConnect from '@infrastructure/connection/redis'

mysqlConnect()
mongoConnect()
redisConnect()

const appPort = parseInt(process.env.APP_PORT as string)
const peerPort = parseInt(process.env.PEER_PORT as string)
const socketPort = parseInt(process.env.SOCKET_PORT as string)

const application = new App(appPort)

PeerServerApp.getPeerServer(peerPort, 'messenger', '/messenger')

const socketServer = new SocketServer('/messenger', socketPort)
const socketConnection = socketServer.getConnection()

// eslint-disable-next-line no-new
new Messenger(socketConnection)

application.run()
socketServer.run()
