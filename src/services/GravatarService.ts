import * as crypto from 'crypto'

export const getAvatarUrl = (email:string, size:number = 80): string => {
  const hash = crypto.createHash('md5').update(email).digest('hex')

  return `https://www.gravatar.com/avatar/${hash}?s=${size}&d=mp&r=g`
}
