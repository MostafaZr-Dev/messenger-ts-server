import * as crypto from 'crypto'
import { v4 as uuid } from 'uuid'
import * as bcrypt from 'bcrypt'

export default class HashService {
  public static randomHash (length = 10): string {
    return crypto.randomBytes(length).toString('hex')
  }

  public static generateID (): string {
    return uuid()
  }

  public static hashPassword (password: string): string {
    return bcrypt.hashSync(password, 10)
  }

  public static comparePassword (hashedPassword: string, password:string): boolean {
    return bcrypt.compareSync(password, hashedPassword)
  }
}
