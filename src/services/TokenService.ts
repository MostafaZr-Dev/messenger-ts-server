import { Request } from 'express'
import * as jwt from 'jsonwebtoken'

export default class TokenService {
  public static sign (data:any): string {
    return jwt.sign(data, process.env.JWT_SECRET as string)
  }

  public static verify (token: string) {
    try {
      return jwt.verify(token, process.env.JWT_SECRET as string)
    } catch (error) {
      return false
    }
  }

  public static findToken (req:Request): string | null {
    const auth = req.headers.authorization

    if (!auth) {
      return null
    }

    const [, token] = auth.split(' ')

    if (!token) {
      return null
    }

    return token
  }
}
