import ICache from './contracts/ICache'
import ICacheConfigurable from './contracts/ICacheConfigurable'
import CacheRedisProvider from './providers/CacheRedisProvider'

type Provider = ICache & Partial<ICacheConfigurable>
export default class CacheProviderFactory {
    private readonly providers: Map<string, Provider> = new Map<string, ICache>()

    public constructor () {
      this.providers.set('redis', new CacheRedisProvider())
    }

    public registerProvider (name: string, provider: Provider): void {
      if (this.has(name)) {
        this.providers.set(name, provider)
      }
    }

    public getProvider (name: string): Provider {
      if (!this.has(name)) {
        throw new Error(`Provider ${name} does not exist!`)
      }

      return this.providers.get(name) as Provider
    }

    private has (name: string): boolean {
      return this.providers.has(name)
    }
}
