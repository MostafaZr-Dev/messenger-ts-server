import CacheProviderFactory from './CacheProviderFactory'

export default class CacheService {
    private readonly chaceProvider: CacheProviderFactory

    public constructor () {
      this.chaceProvider = new CacheProviderFactory()
    }

    public async get (key:string): Promise<any> {
      const provider = this.chaceProvider.getProvider('redis')

      if (provider.config) {
        provider.config({
          key
        })
      }

      const value = await provider.get()

      return value
    }

    public async set (key:string, value: any): Promise<any> {
      const provider = this.chaceProvider.getProvider('redis')

      if (provider.config) {
        provider.config({
          key
        })
      }

      await provider.set(value)
    }
}
