
export default interface ICache{
    get(): any
    set(data: any): void
}
