
import { promisify } from 'util'

import ICache from '../contracts/ICache'
import { redisClient } from '@infrastructure/connection/redis'
import ICacheConfigurable from '../contracts/ICacheConfigurable'

const getAsync = promisify(redisClient.get).bind(redisClient)

export default class CacheRedisProvider implements ICache, ICacheConfigurable {
    private key: string = ''
    private expire: number | null = null

    config (config: any): void {
      this.key = config.key
      this.expire = config.expire
    }

    public async get<T> (): Promise<T> {
      const value = await getAsync(this.key)

      return value ? JSON.parse(value) : null
    }

    public async set (data: any): Promise<void> {
      const value = JSON.stringify(data)
      let expireTime = 5 * 60

      if (this.expire) {
        expireTime = this.expire
      }

      redisClient.expire(this.key, expireTime)
      const setAsync = promisify(redisClient.set).bind(redisClient)

      await setAsync(this.key, value)
    }
}
