import { Socket } from 'socket.io'

export default class ClientManager {
    private clients: Map<string, Socket> = new Map<string, Socket>()

    public addClient (client: Socket): void {
      const clientHash = client.handshake.query.hash as string

      if (!this.clients.has(clientHash)) {
        this.clients.set(clientHash, client)
      }
    }

    public removeClient (client: Socket): void {
      const clientHash = client.handshake.query.hash as string

      if (this.clients.has(clientHash)) {
        this.clients.delete(clientHash)
      }
    }

    public getClient (hash: string): Socket | null {
      if (!this.clients.has(hash)) {
        return null
      }

      return this.clients.get(hash) as Socket
    }

    public allClient (): Map<string, Socket> {
      return this.clients
    }
}
