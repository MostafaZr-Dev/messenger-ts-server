import ClientManager from './ClientManager'

type Handler = (...data:any[]) => void
export default class EventManager {
    private readonly events:Map<string, Handler> = new Map<string, Handler>()
    private readonly clientManager: ClientManager

    public constructor (clientManager: ClientManager) {
      this.clientManager = clientManager
    }

    public $on (event: string, handler:Handler): void {
      if (!this.events.has(event)) {
        this.events.set(event, handler)
      }
    }

    public $emit (event: string, ...data: any[]): void {
      const { to, ...othersData } = data[0]

      const client = this.clientManager.getClient(to)

      if (!client) {
        throw new Error('client does not exist!')
      }

      console.log('server emit: ', event, { othersData })
      client.emit(event, othersData)
    }

    public $run (event: string, ...data: any[]): void {
      if (!this.events.get(event)) {
        throw new Error('event does not exist!')
      }

      (this.events.get(event) as Handler)(...data)
    }
}
