
import EventManager from './EventManager'

export default class MessageHandler {
    private eventManager: EventManager

    public constructor (eventManager: EventManager) {
      this.eventManager = eventManager
    }

    public newChatRequest (...data: any[]): void {
      this.eventManager.$emit('newChatRequest', ...data)
    }

    public newChatResponse (...data: any[]): void {
      this.eventManager.$emit('newChatResponse', ...data)
    }

    public finishChat (...data: any[]): void {
      this.eventManager.$emit('finishChat', ...data)
    }
}
