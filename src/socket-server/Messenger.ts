import { Server, Socket } from 'socket.io'

import ClientManager from './ClientManager'
import EventManager from './EventManager'
import MessageHandler from './MessageHandler'
import OnlineManager from './OnlineManager'

export default class Messenger {
    private readonly socketConnection: Server
    private readonly clientManager: ClientManager
    private readonly onlineManager: OnlineManager
    private readonly eventManager: EventManager
    private readonly messageHandler: MessageHandler
    private client: Socket | null = null

    public constructor (socketConnection: Server) {
      this.socketConnection = socketConnection
      this.clientManager = new ClientManager()
      this.onlineManager = new OnlineManager()
      this.eventManager = new EventManager(this.clientManager)
      this.messageHandler = new MessageHandler(this.eventManager)

      this.initEvents()
      this.socketConnection.on('connection', this.handleConnectClient.bind(this))
      this.socketConnection.engine.on('connection_error', this.handleError.bind(this))
    }

    private initEvents (): void {
      this.eventManager.$on('newChatRequest', this.messageHandler.newChatRequest.bind(this.messageHandler))

      this.eventManager.$on('newChatResponse', this.messageHandler.newChatResponse.bind(this.messageHandler))

      this.eventManager.$on('finishChat', this.messageHandler.finishChat.bind(this.messageHandler))
    }

    private async handleConnectClient (client: Socket) {
      console.log(`socket connected with ID: ${client.id}`)
      this.client = client
      this.clientManager.addClient(client)

      client.on('disconnect', this.handleDisconnectClient.bind(this))
      client.onAny((event:string, ...args: any[]) => {
        console.log(`client emit: ${event}`)
        this.eventManager.$run(event, ...args)
      })

      const {
        hash,
        lat,
        long
      } = client.handshake.query

      await this.onlineManager.addToOnlineUsers(
        hash as string,
        parseFloat(lat as string),
        parseFloat(long as string)
      )

      const onlineUsers = await this.onlineManager.getOnlineUsers()

      console.log('onlineUsers show:', onlineUsers)

      onlineUsers.forEach((onlineUser) => {
        this.eventManager.$emit('onlineUsers', onlineUser)
      })
    }

    private async handleDisconnectClient () {
      this.clientManager.removeClient(this.client as Socket)

      const { hash } = (this.client as Socket).handshake.query
      await this.onlineManager.removeFromOnlineUser(hash as string)

      const onlineUsers = await this.onlineManager.getOnlineUsers()
      console.log({ onlineUsers })
      onlineUsers.forEach((onlineUser) => {
        this.eventManager.$emit('onlineUsers', onlineUser)
      })
    }

    private handleError (error: Error) {
      console.log('error in socket connection', error)
    }
}
