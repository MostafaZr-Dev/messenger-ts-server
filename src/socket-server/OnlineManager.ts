import IOnlineUsersRepository from '@components/online-users/repository/IOnlineUsersRepository'
import OnlineUsersMongoRepository from '@components/online-users/repository/OnlineUsersMongoRepository'
import IUserRepository from '@components/users/repository/IUserRepository'
import UserMysqlRepository from '@components/users/repository/UserMysqlRepository'
import { getAvatarUrl } from '@services/GravatarService'
import CacheService from '@services/cache/CacheService'
import IOnlineUser from '@components/online-users/model/IOnlineUser'

export default class OnlineManager {
    private readonly usersRepository: IUserRepository
    private readonly onlineUsersRepository: IOnlineUsersRepository
    private readonly cacheService: CacheService

    public constructor () {
      this.usersRepository = new UserMysqlRepository()
      this.onlineUsersRepository = new OnlineUsersMongoRepository()
      this.cacheService = new CacheService()
    }

    public async addToOnlineUsers (hash: string, latitude:number, longitude: number): Promise<boolean> {
      const user = await this.usersRepository.findByHash(hash)

      if (!user) {
        throw new Error(`there is no user with hash ${hash}`)
      }

      const isOnlineUserExist = await this.onlineUsersRepository.findByHash(hash)

      if (isOnlineUserExist) {
        return true
      }

      await this.onlineUsersRepository.create({
        user: {
          fullName: user.fullName,
          hash: user.hash,
          avatar: getAvatarUrl(user.email)
        },
        location: {
          type: 'Point',
          coordinates: [
            latitude,
            longitude
          ]
        }
      })

      return true
    }

    public async removeFromOnlineUser (hash:string): Promise<boolean> {
      return this.onlineUsersRepository.removeByHash(hash)
    }

    public async getOnlineUsers (): Promise<{to:string, onlineUsers: IOnlineUser[]}[]> {
      const onlineUsers = await this.onlineUsersRepository.findAll({})

      if (!onlineUsers.length) {
        return []
      }

      return Promise.all(
        onlineUsers.map(async (onlineUser) => {
          const userLocation = onlineUser.location.coordinates
          const userHash = onlineUser.user.hash
          // eslint-disable-next-line no-unused-vars
          const cacheKey = `online-user-near-by-${userHash}`
          // const cachedOnlineUsers = await this.cacheService.get(cacheKey)
          const cachedOnlineUsers = null

          if (cachedOnlineUsers) {
            return {
              to: userHash,
              onlineUsers: cachedOnlineUsers
            }
          }

          const onlineUsers = await this.onlineUsersRepository.findAll(
            {
              location: userLocation
            },
            {
              _id: 0,
              user: 1,
              location: 1
            }
          )

          if (!onlineUsers.length) {
            return {
              to: userHash,
              onlineUsers: []
            }
          }

          // await this.cacheService.set(cacheKey, onlineUsers)

          return {
            to: userHash,
            onlineUsers: onlineUsers
          }
        })
      )
    }
}
