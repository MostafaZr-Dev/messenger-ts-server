import * as https from 'https'
import * as express from 'express'
import { Server } from 'socket.io'
import * as fs from 'fs'
import * as path from 'path'
import * as cors from 'cors'

const privateKey = fs.readFileSync(path.resolve(process.cwd(), '.cert/key.pem'), 'utf-8')
const certificate = fs.readFileSync(path.resolve(process.cwd(), '.cert/cert.pem'), 'utf-8')

export default class SocketServer {
    private httpsServer: https.Server
    private server: Server
    private app: express.Application
    private port: number

    constructor (path:string, port:number = 8000) {
      this.port = port
      this.app = express()
      this.app.use(cors())

      this.httpsServer = https.createServer({
        key: privateKey,
        cert: certificate
      }, this.app)

      this.server = new Server(this.httpsServer, {
        path,
        cors: {
          origin: '*'
        }
      })
    }

    public getConnection (): Server {
      return this.server
    }

    public run (): void {
      this.httpsServer.listen(this.port)
    }
}
